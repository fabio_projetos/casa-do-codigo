package br.com.casadocodigo.loja.models;

import java.math.BigDecimal;

public class DadosPagamento {
	
	private BigDecimal value;
	
	// Caso precisamos instanciar receber o valor no construtor
	public DadosPagamento() {}
	
	public DadosPagamento(BigDecimal valor) {
		this.value = valor;
	}
		
	public BigDecimal getValue() {
		return value;
	}
}