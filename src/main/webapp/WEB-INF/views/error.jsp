<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<tags:pageTemplate titulo="Erro">
	<section id="index-section" class="container middle">
		<h2>N�o foi poss�vel realizar essa opera��o</h2>
		
		<!-- 
			Mensagem: ${exception.message}				
			<c:forEach items="${exception.stackTrace}" var="stk">
				${stk}
			</c:forEach>
		-->		
	</section>
</tags:pageTemplate>
